const express = require('express');
const bodyParser = require ('body-parser');
const app = express();
app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());
const port = 8000;

//habilitar servidor
app.listen(port, ()=>{
    console.log("Projeto rodando na porta: " + port);
});

app.get('/aluno',(req,res)=>{
    res.send("{message:aluno encontrado}");
});

//recurso de request.query
app.get('/aluno/filtros',(req,res)=>{
    let source = req.query;
    let ret = "Dados solicitados: " + source.nome + " " + source.sobrenome;
    res.send("{message:"+ret+"}");
});

//recurso de request.param
app.get('/aluno/pesquisa/:valor',(req, res)=>{
    let dado = req.params.valor;
    let ret = "Dados solicitado:"+dado;
    res.send("{message:"+ret+"}");
});

//recurso de request.body
app.post('/aluno',(req, res)=>{
    let dados = req.body;
    let headers_ = req.headers["access"];
    console.log("Valor Access: " + headers_);
    let ret = "Dados enviados: Nome:" + dados.nome;
    ret+=" Sobrenome: " + dados.sobrenome;
    ret+=" Idade: " + dados.idade;
    res.send("{message:"+ret+"}");
});